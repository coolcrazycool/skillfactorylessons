"""
Game is presented here guess the number in range 1 to 100. User should find it.
Game will give you information - more or less.

Task: find less than 20 moves.
"""

import random

# Const values
UPPER = 100
DOWN = 1


# return random number in range [1, 100]
def number_guess() -> int:
    return random.randint(DOWN, UPPER)


class Guesser(object):
    """
    Class that find value
        up_value - up border
        down_value - down border
    """

    def __init__(self):
        self.up_value = UPPER
        self.down_value = DOWN
        self.steps = 0

    # Property computes mean value to solve task
    @property
    def guess(self) -> int:
        return (self.up_value + self.down_value) // 2

    # Set Upper border
    def set_upper(self, value: int):
        self.steps += 1
        self.up_value = value

    # Set Down border
    def set_down(self, value: int):
        self.steps += 1
        self.down_value = value


if __name__ == "__main__":
    guessed_number = number_guess()
    guesser = Guesser()

    # Set border or exit while
    while True:
        print(guesser.down_value, guesser.guess, guesser.up_value, guessed_number)
        if guesser.guess == guessed_number:
            break
        elif guesser.guess > guessed_number:
            guesser.set_upper(guesser.guess)
        else:
            guesser.set_down(guesser.guess)

    print(f"Found number: {guesser.guess} == computer number: {guessed_number}. {guesser.steps} steps")
